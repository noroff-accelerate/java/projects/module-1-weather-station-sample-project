# Weather Station

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Sample project to read and process weather data.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This is a console application that demonstrates the concepts covered in _Module 1_ of the Java Full Stack course.

It aims to demonstrate best practices when using these concepts. The application mainly focuses on OOP, File IO, and Stream API.

The application is designed to be readable and easily extensible. Currently, only __Excel__ data can be read but more reader types can easily be extended through implementing `WeatherDataReader` interface.

A few design patterns have been implemented in this solution:

- Factory
- Builder
- Strategy (`WeatherDataReader`)

DRY programming has also been implemented where feasible.

The project is created with a __Maven__ build system. Build systems have not been covered at this point in the course. When working with _Spring_, we use build systems. Their purpose is to simplify using external dependencies. The `pom.xml` file contains the configuration, do not change it.

## Install

- Install JDK 17
- Install Intellij
- Clone repository

## Usage

- Run the application
- Download more Excel weather data from [here](https://meteostat.net/en/place/4V90UQ?t=2021-10-01/2021-10-31)
  - Make sure to download monthly data and fill in any blanks. The Apache POI library doesnt seem to replace blanks when it should.
- Place in `resources` folder to be searchable by the readers.
- Build a new weather station, specify data with `.withExcelData(String path)`.

## Maintainers

[@NicholasLennox](https://gitlab.com/NicholasLennox)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate
