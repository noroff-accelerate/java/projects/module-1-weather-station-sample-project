import org.apache.commons.math3.util.Pair;
import util.CityWeatherStationBuilder;
import util.WeatherStationFactory;
import weather.stations.WeatherStation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        // Using factory method pattern
        WeatherStation osloOct2020 = WeatherStationFactory.createOsloWeatherStationOct2020();
        // Using builder pattern
        WeatherStation osloOct2021 =
                new CityWeatherStationBuilder("Oslo", "NO")
                        .atElevation(26)
                        .inTimeZone("Europe/Oslo")
                        .atCoords(59.9127,10.7461)
                        .withExcelData("weather_oslo_oct2021.xlsx")
                        .build();

        // Comparing 2020 to 2021
        System.out.println("October 2020:");
        System.out.println("-----------------------");
        displayStationStats(osloOct2020);
        System.out.println("-----------------------");
        System.out.println("October 2021:");
        System.out.println("-----------------------");
        displayStationStats(osloOct2021);
        System.out.println("-----------------------");
    }

    public static void displayStationStats(WeatherStation station) {
        System.out.println("Station: " + station.getDetails());
        System.out.println("Average temp: " + formatSingleDecimalPlace(station.getAverageTemperature()) + " C");
        Pair<LocalDate, Double> hottest = station.getMaximumTemperature();
        System.out.println("Hottest day: " + formatSingleDecimalPlace(hottest.getSecond()) + " C, " + formatLocalDate(hottest.getFirst()));
        Pair<LocalDate, Double> coldest = station.getMinimumTemperature();
        System.out.println("Coldest day: " + formatSingleDecimalPlace(coldest.getSecond()) + " C, " + formatLocalDate(coldest.getFirst()));
        System.out.println("Average rainfall (mm): " + formatSingleDecimalPlace(station.getAverageRainfall()));
        System.out.println("Total rainfall (mm): " + formatSingleDecimalPlace(station.getTotalRainfall()));
        Pair<LocalDate, Double> rainiest = station.getMaximumRainfall();
        System.out.println("Wettest day: " + formatSingleDecimalPlace(rainiest.getSecond()) + " mm, " + formatLocalDate(rainiest.getFirst()));
        System.out.println("Average wind speed (km/h): " + formatSingleDecimalPlace(station.getAverageWindSpeed()));
        double avgWindDir = station.getAverageWindDirection();
        System.out.println("Average wind direction (degrees): " + formatSingleDecimalPlace(avgWindDir) + " (" + convertDegreeToDirection(avgWindDir) + ")");
        Pair<LocalDate, Double> windiest = station.getMaximumWindGust();
        System.out.println("Windiest day: " + formatSingleDecimalPlace(windiest.getSecond()) + " km/h, " + formatLocalDate(windiest.getFirst()));
        System.out.println("Average air pressure: " + formatSingleDecimalPlace(station.getAverageAirPressure()));
        Pair<LocalDate, Double> snowiest = station.getMaximumSnowfall();
        System.out.println("Snowiest day: " + formatSingleDecimalPlace(snowiest.getSecond()) + " mm, " + formatLocalDate(snowiest.getFirst()));
    }

    public static String formatSingleDecimalPlace(Number number) {
        return String.format("%.1f",number);
    }

    public static String formatLocalDate(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu"));
    }

    // Based on: http://snowfence.umn.edu/Components/winddirectionanddegrees.htm
    // and: https://www.campbellsci.com/blog/convert-wind-directions
    public static String convertDegreeToDirection(double direction) {
        String[] directions = {"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW","N"};
        return directions[(int)(direction%360/22.5) + 1];
    }
}
