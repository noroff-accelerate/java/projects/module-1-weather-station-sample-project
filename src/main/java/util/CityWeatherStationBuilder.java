package util;

import weather.datareaders.DefaultWeatherDataReader;
import weather.datareaders.ExcelWeatherDataReader;
import weather.datareaders.WeatherDataReader;
import weather.stations.CityWeatherStation;

import java.awt.geom.Point2D;

// A builder pattern chains in the same way Stream API does.
// Builders exist to help creation of complex objects with different ways to construct

// If our stations change how they are constructed we can handle it here, not having to change clients.

public class CityWeatherStationBuilder {
    private String city;
    private String country;
    private double elevation = 0;
    private String timeZone = "N/A";
    private Point2D.Double coords = new Point2D.Double(0,0);
    // Need some default value
    private WeatherDataReader weatherDataReader = new DefaultWeatherDataReader();

    // All required params are in constructor, optional ones are encapsulated in builder methods
    public CityWeatherStationBuilder(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public CityWeatherStationBuilder atElevation(double elevation) {
        this.elevation = elevation;
        return this;
    }

    public CityWeatherStationBuilder inTimeZone(String timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    public CityWeatherStationBuilder atCoords(double lat, double lng) {
        coords = new Point2D.Double(lat,lng);
        return this;
    }

    public CityWeatherStationBuilder withExcelData(String path) {
        weatherDataReader = new ExcelWeatherDataReader(path);
        return this;
    }

    public CityWeatherStation build() {
        // The actual product is created here based on our configuration and returned
        return new CityWeatherStation(city,country,elevation,timeZone,coords,weatherDataReader);
    }
}
