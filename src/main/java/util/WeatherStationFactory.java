package util;

import weather.stations.CityWeatherStation;
import weather.datareaders.ExcelWeatherDataReader;
import weather.stations.WeatherStation;

import java.awt.geom.Point2D;

//* Encapsulates the creation logic for our Weather stations, and removes the concrete CityWeatherStation dependency from Main. /

public class WeatherStationFactory {
    public static WeatherStation createOsloWeatherStationOct2021() {
        return new CityWeatherStation(
                "Oslo",
                "Norway",
                26,
                "Europe/Oslo",
                new Point2D.Double(59.9127,10.7461),
                new ExcelWeatherDataReader("weather_oslo_oct2021.xlsx"));
    }

    public static WeatherStation createOsloWeatherStationOct2020() {
        return new CityWeatherStation(
                "Oslo",
                "Norway",
                26,
                "Europe/Oslo",
                new Point2D.Double(59.9127,10.7461),
                new ExcelWeatherDataReader("weather_oslo_oct2020.xlsx"));
    }
}
