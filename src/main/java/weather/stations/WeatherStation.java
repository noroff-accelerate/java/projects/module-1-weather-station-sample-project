package weather.stations;

import org.apache.commons.math3.util.Pair;

import java.time.LocalDate;

//* A Weather station has a location and a list of captured data points. Each point represents a day. /

public interface WeatherStation {
     double getAverageTemperature();
     Pair<LocalDate, Double> getMaximumTemperature();
     Pair<LocalDate, Double> getMinimumTemperature();
     double getAverageRainfall();
     double getTotalRainfall();
     Pair<LocalDate, Double> getMaximumRainfall();
     double getAverageWindSpeed();
     double getAverageWindDirection();
     Pair<LocalDate, Double> getMaximumWindGust();
     double getAverageAirPressure();
     Pair<LocalDate, Double> getMaximumSnowfall();
     double getTotalSnowfall();
     String getDetails();
}
