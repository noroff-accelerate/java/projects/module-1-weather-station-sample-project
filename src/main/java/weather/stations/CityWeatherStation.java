package weather.stations;

import org.apache.commons.math3.util.Pair;
import weather.datareaders.WeatherDataReader;
import weather.data.WeatherDataPoint;

import java.awt.geom.Point2D;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class CityWeatherStation implements WeatherStation {

    private List<WeatherDataPoint> weatherData;
    private String city;
    private String country;
    private double elevation;
    private String timeZone;
    private Point2D.Double coords;
    private WeatherDataReader weatherDataReader;

    public CityWeatherStation(String city,
                              String country, double elevation, String timeZone,
                              Point2D.Double coords, WeatherDataReader weatherDataReader) {
        this.city = city;
        this.country = country;
        this.elevation = elevation;
        this.timeZone = timeZone;
        this.coords = coords;
        this.weatherDataReader = weatherDataReader;
        loadWeatherData();
    }

    public void loadWeatherData() {
        weatherData = weatherDataReader.readWeatherData();
    }

    // Methods to help not repeating ourselves
    public double getAverage(DoubleStream stream) {
        return stream
                .average()
                .getAsDouble();
    }

    public double getMaximum(DoubleStream stream) {
        return stream
                .max()
                .getAsDouble();
    }

    public double getMinimum(DoubleStream stream) {
        return stream
                .min()
                .getAsDouble();
    }

    public LocalDate getDateFromFirst(Stream<WeatherDataPoint> stream) {
        return stream
                .findFirst()
                .get()
                .date();
    }


    @Override
    public double getAverageTemperature() {
        return getAverage(weatherData.stream()
                .mapToDouble(data -> data.tAvg()));
    }

    @Override
    public double getAverageRainfall() {
        return getAverage(weatherData.stream()
                .mapToDouble(data -> data.prcp()));
    }

    @Override
    public double getAverageWindSpeed() {
        return getAverage(weatherData.stream()
                .mapToDouble(data -> data.wspd()));
    }

    @Override
    public double getAverageWindDirection() {
        return getAverage(weatherData.stream()
                .mapToDouble(data -> data.wdir()));
    }


    @Override
    public double getAverageAirPressure() {
        return getAverage(weatherData.stream()
                .mapToDouble(data -> data.pres()));
    }

    @Override
    public Pair<LocalDate, Double> getMaximumTemperature() {
        double max = getMaximum(weatherData.stream()
                .mapToDouble(data -> data.tMax()));

        return new Pair<>(
                getDateFromFirst(weatherData.stream().filter(d -> d.tMax() == max)),
                max
        );
    }

    @Override
    public Pair<LocalDate, Double> getMinimumTemperature() {
        double min = getMinimum(weatherData.stream()
                .mapToDouble(data -> data.tMin()));

        return new Pair<>(
                getDateFromFirst(weatherData.stream().filter(d -> d.tMin() == min)),
                min
        );
    }

    @Override
    public double getTotalRainfall() {
        return weatherData.stream()
                .mapToDouble(data -> data.prcp())
                .sum();
    }

    @Override
    public Pair<LocalDate, Double> getMaximumRainfall() {
        double max = getMaximum(weatherData.stream()
                .mapToDouble(data -> data.prcp()));

        return new Pair<>(
                getDateFromFirst(weatherData.stream().filter(d -> d.prcp() == max)),
                max
        );
    }

    @Override
    public Pair<LocalDate, Double> getMaximumWindGust() {
        double max = getMaximum(weatherData.stream()
                .mapToDouble(data -> data.wpgst()));

        return new Pair<>(
                getDateFromFirst(weatherData.stream().filter(d -> d.wpgst() == max)),
                max
        );
    }

    @Override
    public Pair<LocalDate, Double> getMaximumSnowfall() {
        double max = getMaximum(weatherData.stream()
                .mapToDouble(data -> data.snow()));

        return new Pair<>(
                getDateFromFirst(weatherData.stream().filter(d -> d.snow() == max)),
                max
        );
    }

    @Override
    public double getTotalSnowfall() {
        return weatherData.stream()
                .mapToDouble(WeatherDataPoint::snow)
                .sum();
    }

    @Override
    public String getDetails() {
        return city + " " + country;
    }
}
