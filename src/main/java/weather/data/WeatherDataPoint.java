package weather.data;

import java.time.LocalDate;

// A record is used to pass immutable data easily as an alternative to a class with a lot of boiler plate code.
// Article on records: https://www.baeldung.com/java-record-keyword

public record WeatherDataPoint(
         LocalDate date,
         double tAvg, // Average temperature
         double tMin, // Minimum temperature
         double tMax, // Maximum temperature
         double prcp, // mm of rain
         double snow, // mm of snow
         double wdir, // Wind direction in degrees
         double wspd, // Wind speed
         double wpgst, // Wind peak gust
         double pres // Air pressure in KPA
        ) { }