package weather.datareaders;

import weather.datareaders.WeatherDataReader;
import weather.data.WeatherDataPoint;

import java.time.LocalDate;
import java.util.List;

public class DefaultWeatherDataReader implements WeatherDataReader {
    @Override
    public List<WeatherDataPoint> readWeatherData() {
        return List.of(new WeatherDataPoint(LocalDate.MIN,0,0,0,0,0,0,0,0,0));
    }
}
