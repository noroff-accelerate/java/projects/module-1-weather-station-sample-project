package weather.datareaders;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import weather.data.WeatherDataPoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

// Reading excel data is based on:
// https://howtodoinjava.com/java/library/readingwriting-excel-files-in-java-poi-tutorial/

// The data used for this can be downloaded from:
// https://meteostat.net/en/place/4V90UQ?t=2020-10-01/2020-10-31

public class ExcelWeatherDataReader implements WeatherDataReader {

    private List<WeatherDataPoint> data = new ArrayList<>();
    private String path;

    public ExcelWeatherDataReader(String path) {
        this.path = path;
    }

    @Override
    public List<WeatherDataPoint> readWeatherData() {

        // Reading a file from dedicated resources folder using .getResource()
        try(FileInputStream file = new FileInputStream(
                new File(ExcelWeatherDataReader.class.getClassLoader()
                        .getResource(path).getPath()))) {

            // Create Workbook instance holding reference to .xlsx file
            Workbook workbook = new XSSFWorkbook(file);

            // Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            // Iterate through each row
            Iterator<Row> rowIterator = sheet.iterator();
            int rowNum = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                // Skipping headers
                if (row.getRowNum() == 0 && rowIterator.hasNext()) {
                    row = rowIterator.next();
                }

                // We know each row has the following columns:
                // 0-date-string, 1-tavg-num, 2-tmin-num, 3-tmax-num, 4-prcp-num, 5-snow-num,
                // 6-wdir-num, 7-wspd-num, 8-wpgt-num, 9-pres-num

                // Create temp WeatherData to map into
                WeatherDataPoint dataPoint = new WeatherDataPoint(
                        LocalDate.parse(row.getCell(0).getStringCellValue()),
                        row.getCell(1).getNumericCellValue(),
                        row.getCell(2).getNumericCellValue(),
                        row.getCell(3).getNumericCellValue(),
                        row.getCell(4).getNumericCellValue(),
                        row.getCell(5).getNumericCellValue(),
                        row.getCell(6).getNumericCellValue(),
                        row.getCell(7).getNumericCellValue(),
                        row.getCell(8).getNumericCellValue(),
                        row.getCell(9).getNumericCellValue()
                );

                // Add to our weather points
                data.add(dataPoint);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
