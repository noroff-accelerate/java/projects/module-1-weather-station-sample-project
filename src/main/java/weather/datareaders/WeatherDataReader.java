package weather.datareaders;

import weather.data.WeatherDataPoint;

import java.util.List;

public interface WeatherDataReader {
    List<WeatherDataPoint> readWeatherData();
}
